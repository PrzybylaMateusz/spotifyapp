using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpotifyAPI.Web;
using SpotifyApp.API.Dtos;

namespace SpotifyApp.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        public static SpotifyClientConfig DefaultConfig = SpotifyClientConfig.CreateDefault();

        [AllowAnonymous]
        [HttpGet("{tokenSpotify}")]
        public async Task<IActionResult> GetTrack(string tokenSpotify)
        {
            var config = DefaultConfig.WithToken(tokenSpotify);
            var spotify = new SpotifyClient(config);

            var context = await spotify.Player.GetCurrentlyPlaying(new PlayerCurrentlyPlayingRequest());

            if(!context.IsPlaying)
            {
                return Ok(null);
            }

            var currentAlbum = context.Item;

            if (currentAlbum is FullTrack track)
            {
                var album = new AlbumDto()
                {
                    Artist = string.Join(",", track.Artists[0].Name),
                    Name = track.Album.Name,
                    Id = track.Album.Id,
                    UserId = 1,
                    CoverUrl = track.Album.Images[0].Url,
                    Year = track.Album.ReleaseDate.Substring(0, 4),
                    ArtistId = track.Artists[0].Id,
                };

                var currentlyPlayed = new CurrentlyPlayedDto()
                {
                    TrackName = track.Name,
                    Album = album
                };

                return Ok(currentlyPlayed);
            }
            else
            {
                return Ok(null);
            }
        }
    }
}