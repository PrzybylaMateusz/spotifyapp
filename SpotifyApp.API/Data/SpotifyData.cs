using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpotifyAPI.Web;
using SpotifyApp.API.Dtos;
using Microsoft.Extensions.Configuration;

namespace SpotifyApp.API.Data
{
    public class SpotifyData : ISpotifyData
    {
        public SpotifyData(IConfiguration configuration)
        {
            Configuration = configuration;
            ClientId = Configuration["SPOTIFY_CLIENT_ID"];
            SecretId = Configuration["SPOTIFY_CLIENT_SECRET"];
        }

        public IConfiguration Configuration { get; }

        private string ClientId { get; }
        private string SecretId { get; }

        public static SpotifyClientConfig DefaultConfig = SpotifyClientConfig.CreateDefault();

        public async Task<IEnumerable<AlbumDto>> SearchSpotifyAlbums(string keyword)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var searchItem = await spotify.Search.Item(new SearchRequest(SearchRequest.Types.Album, keyword));

            var albumsToReturn = new List<AlbumDto>();

            if (searchItem.Albums.Items != null)
            {
                albumsToReturn.AddRange(searchItem.Albums.Items.Select(album => new AlbumDto
                {
                    Artist = album.Artists[0].Name,
                    Name = album.Name,
                    Id = album.Id,
                    UserId = 1,
                    CoverUrl = album.Images[0].Url,
                    Year = album.ReleaseDate.Substring(0, 4)
                }));
            }

            return albumsToReturn;
        }

        public async Task<AlbumDto> GetSpotifyAlbum(string id)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var albumFromSpotify = await spotify.Albums.Get(id);
       
            return new AlbumDto()
            {
                 Artist = string.Join(",", albumFromSpotify.Artists.Select((x) => x.Name)),
                 Name = albumFromSpotify.Name,
                 Id = albumFromSpotify.Id,
                 UserId = 1,
                 CoverUrl = albumFromSpotify.Images[0].Url,
                 Year = albumFromSpotify.ReleaseDate.Substring(0, 4),
                 ArtistId = albumFromSpotify.Artists[0].Id,
            };

        }

        public async Task<ArtistWithAlbumsDto> GetSpotifyArtist(string id)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var artistFromSpotify = await spotify.Artists.Get(id);

            var albumsForArtist = await spotify.Artists.GetAlbums(id);

            var albumsToReturn = new List<AlbumDto>();
            if (albumsForArtist.Items != null)
            {
                albumsToReturn.AddRange(albumsForArtist.Items.Select(album => new AlbumDto
                {
                    Artist = artistFromSpotify.Name,
                    Name = album.Name,
                    Id = album.Id,
                    CoverUrl = album.Images[0].Url,
                    UserId = 1,
                    Year = album.ReleaseDate.Substring(0, 4)
                }));
            }

            return new ArtistWithAlbumsDto()
            {
                Name = artistFromSpotify.Name,
                Id = artistFromSpotify.Id,
                PhotoUrl = artistFromSpotify.Images[0].Url,
                Albums = albumsToReturn
            };
        }

        public async Task<IEnumerable<AlbumDto>> GetSpotifyAlbums(List<string> albumsIdToGet)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var albumsFromSpotify = await spotify.Albums.GetSeveral(new AlbumsRequest(albumsIdToGet));

            var albumsToReturn = new List<AlbumDto>();

            foreach (var album in albumsFromSpotify.Albums)
            {
                var albumToReturn = new AlbumDto
                {
                    Artist = album.Artists[0].Name,
                    ArtistId = album.Artists[0].Id,
                    Name = album.Name,
                    Id = album.Id,
                    UserId = 1,
                    CoverUrl = album.Images[0].Url,
                    Year = album.ReleaseDate.Substring(0, 4)
                };
                albumsToReturn.Add(albumToReturn);
            }

            return albumsToReturn;
        }

         public async Task<IEnumerable<ArtistDto>> GetSpotifyArtists(List<string> artistsIdToGet)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var artistsFromSpotify = await spotify.Artists.GetSeveral(new ArtistsRequest(artistsIdToGet));

            var artistsToReturn = new List<ArtistDto>();

            foreach (var artist in artistsFromSpotify.Artists)
            {
                var artistToReturn = new ArtistDto
                {
                    Name = artist.Name, Id = artist.Id, PhotoUrl = artist.Images[0].Url
                };
                artistsToReturn.Add(artistToReturn);
            }

            return artistsToReturn;
        }

         public async Task<IEnumerable<ArtistDto>> SearchSpotifyArtists(string keyword)
        {
            var config = DefaultConfig.WithAuthenticator(new ClientCredentialsAuthenticator(ClientId, SecretId));
            var spotify = new SpotifyClient(config);

            var searchItem = await spotify.Search.Item(new SearchRequest(SearchRequest.Types.Artist, keyword));

            var artistsToReturn = new List<ArtistDto>();

            if (searchItem.Artists.Items == null)
            {
                return artistsToReturn;
            }

            foreach (var artist in searchItem.Artists.Items)
            {
                var artistToReturn = new ArtistDto
                {
                    Name = artist.Name,
                    Id = artist.Id,
                    PhotoUrl = artist.Images.Count > 0 ? artist.Images[0].Url : null
                };
                artistsToReturn.Add(artistToReturn);
            }

            return artistsToReturn;
        }
    }
}